package com.br.porta.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
public class PortaMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortaMsApplication.class, args);
	}

}
