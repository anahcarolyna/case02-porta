package com.br.porta.ms.controller;

import com.br.porta.ms.models.Porta;
import com.br.porta.ms.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.sound.sampled.Port;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta create(@RequestBody Porta porta) {
        Porta portaObjeto = new Porta();

        portaObjeto = portaService.create(porta);

        return portaObjeto;
    }

    @GetMapping("/{id}")
    public Porta getById(@PathVariable Long id) {
        return portaService.getById(id);
    }
}
