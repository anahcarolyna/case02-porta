package com.br.porta.ms.service;

import com.br.porta.ms.exceptions.PortaNaoEncontradaException;
import com.br.porta.ms.models.Porta;
import com.br.porta.ms.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta create(Porta porta){
        return portaRepository.save(porta);
    }

    public Porta getById(Long id){
        Optional<Porta> byId = portaRepository.findById(id);

        if(!byId.isPresent()) {
            throw new PortaNaoEncontradaException();
        }

        return byId.get();
    }
}
