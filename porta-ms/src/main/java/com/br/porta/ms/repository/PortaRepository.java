package com.br.porta.ms.repository;

import com.br.porta.ms.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Long> {
}
